/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//url:https://www.easyjet.com/fr/secure/MyEasyJet.mvc/AllBookings
//name: All bookings

var data = [];

var buttonClick = document.getElementById("ensCloseBanner"); // Click button accept cookies

buttonClick.click();

var parent = document.getElementById("allBookingItems");
var children = parent.children;

for(var i=0; i<children.length; i++){
    var destination = children[i].innerHTML;

    var bookingId = destination.match(/<h2>(.*?)<\/h2>/g).map(function(val){
                               return val.replace(/<\/?h2>/g,'');
                            });

    var result = destination.match(/<b>(.*?)<\/b>/g).map(function(val){
           return val.replace(/<\/?b>/g,'').replace(/<\/?span>/g,'');
        });

    data.push({
            title: `Booking`,
            type: "Booking",
            value: bookingId[0],
            data: result
        });

}

Injector.promiseReceive(JSON.stringify(data));